#!/usr/bin/env bash

cd ~/work/tangobox/src/tangobox/scripts

./install-docker.sh

./deploy-containers.sh
